package com.ozimos.test.util

import androidx.recyclerview.widget.DiffUtil
import com.ozimos.test.domain.model.BaseModel

class DiffUtilNonPaging3(
    private val oldItems: List<BaseModel>,
    private val newItems: List<BaseModel>
) :
    DiffUtil.Callback() {
    override fun getOldListSize(): Int = oldItems.size

    override fun getNewListSize(): Int = newItems.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldItems[oldItemPosition] == newItems[newItemPosition]
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldItems[oldItemPosition].id == newItems[newItemPosition].id
    }
}