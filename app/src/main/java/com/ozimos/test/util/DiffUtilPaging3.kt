package com.ozimos.test.util

import androidx.recyclerview.widget.DiffUtil
import com.ozimos.test.domain.model.BaseModel

class DiffUtilPaging3<MODEL : BaseModel> : DiffUtil.ItemCallback<MODEL>() {
    override fun areItemsTheSame(oldItem: MODEL, newItem: MODEL): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: MODEL, newItem: MODEL): Boolean {
        return oldItem.id == newItem.id
    }
}