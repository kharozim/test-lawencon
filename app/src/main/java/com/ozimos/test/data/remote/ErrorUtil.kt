package com.ozimos.test.data.remote

import org.json.JSONObject

object ErrorUtil {
    fun getErrorMessage(errorResponse: String): String {
        return try {
            val responseError = JSONObject(errorResponse)
            responseError.get("status_message").toString()
        } catch (e: Exception) {
            "Error Not found"
        }

    }
}