package com.ozimos.test.data.payload.response

import com.google.gson.annotations.SerializedName
import com.ozimos.test.domain.model.AuthorDetailModel
import com.ozimos.test.domain.model.ReviewModel

data class ReviewResponse(

    @field:SerializedName("author_details")
    val authorDetails: AuthorDetailResponse? = null,

    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("author")
    val author: String? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("content")
    val content: String? = null,

    @field:SerializedName("url")
    val url: String? = null
) {
    fun toModel() = ReviewModel(
        authorDetails = authorDetails?.toModel() ?: AuthorDetailModel(),
        updatedAt = updatedAt ?: "",
        author = author ?: "",
        createdAt = createdAt ?: "",
        id = id ?: "",
        content = content ?: "",
        url = url ?: ""
    )
}

data class AuthorDetailResponse(

    @field:SerializedName("avatar_path")
    val avatarPath: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("rating")
    val rating: Double? = null,

    @field:SerializedName("username")
    val username: String? = null
) {
    fun toModel() = AuthorDetailModel(
        avatarPath = avatarPath ?: "",
        name = name ?: "",
        rating = rating ?: 0.0,
        username = username ?: ""
    )
}

