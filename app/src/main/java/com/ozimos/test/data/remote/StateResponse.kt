package com.ozimos.test.data.remote

sealed class StateResponse<out T : Any> {
    data class Success<out T : Any>(val data: T) : StateResponse<T>()
    data class Failed(val message: String) : StateResponse<Nothing>()
}


/**
 * extension function
 * `true` if [ResultData] is of type [Success] & holds non-null [Success.data].
 */

val StateResponse<*>.succeeded
    get() = this is StateResponse.Success
