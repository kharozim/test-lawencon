package com.ozimos.test.data.payload.response

import com.google.gson.annotations.SerializedName
import com.ozimos.test.domain.model.PagingModel

data class PagingResponse<DATA>(

    @field:SerializedName("page")
    val page: Int? = null,

    @field:SerializedName("total_pages")
    val totalPages: Int? = null,

    @field:SerializedName("results")
    val results: DATA? = null,

    @field:SerializedName("total_results")
    val totalResults: Int? = null
) {
    fun toModel(): PagingModel = PagingModel(
        page = page ?: 0,
        totalPages = totalPages ?: 0,
        totalResults = totalResults ?: 0,
        results = results,
    )
}