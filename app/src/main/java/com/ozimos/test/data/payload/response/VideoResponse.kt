package com.ozimos.test.data.payload.response

import com.google.gson.annotations.SerializedName
import com.ozimos.test.domain.model.VideoItemItemModel
import com.ozimos.test.domain.model.VideoModel

data class VideoResponse(

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("results")
    val results: List<VideoItemItemResponse>? = null
) {
    fun toModel() = VideoModel(
        id = id ?: 0,
        results = results?.asSequence()?.map { it.toModel() }?.toList() ?: emptyList()
    )
}

data class VideoItemItemResponse(

    @field:SerializedName("site")
    val site: String? = null,

    @field:SerializedName("size")
    val size: Int? = null,

    @field:SerializedName("iso_3166_1")
    val iso31661: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("official")
    val official: Boolean? = null,

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("type")
    val type: String? = null,

    @field:SerializedName("published_at")
    val publishedAt: String? = null,

    @field:SerializedName("iso_639_1")
    val iso6391: String? = null,

    @field:SerializedName("key")
    val key: String? = null
) {
    fun toModel() = VideoItemItemModel(
        site = site ?: "",
        size = size ?: 0,
        iso31661 = iso31661 ?: "",
        name = name ?: "",
        official = official ?: false,
        id = id ?: "",
        type = type ?: "",
        publishedAt = publishedAt ?: "",
        iso6391 = iso31661 ?: "",
        key = key ?: ""
    )
}



