package com.ozimos.test.data.payload.response

import com.ozimos.test.domain.model.GenreItemModel
import com.ozimos.test.domain.model.GenreModel

data class GenreResponse(
    val genres: List<GenreItemResponse>? = null
) {
    fun toModel(): GenreModel = GenreModel(
        genres = genres?.asSequence()?.map { it.toModel() }?.toList() ?: emptyList()
    )
}

data class GenreItemResponse(
    val id: String? = null,
    val name: String? = null
) {
    fun toModel(): GenreItemModel = GenreItemModel(
        id = id ?: "0",
        name = name ?: ""
    )
}