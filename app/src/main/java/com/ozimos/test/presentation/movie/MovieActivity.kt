package com.ozimos.test.presentation.movie

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.paging.PagingData
import com.google.gson.Gson
import com.ozimos.test.R
import com.ozimos.test.databinding.ActivityMovieBinding
import com.ozimos.test.domain.model.GenreItemModel
import com.ozimos.test.domain.model.MovieModel
import com.ozimos.test.presentation.MovieViewModel
import com.ozimos.test.presentation.detail.DetailMovieActivity
import com.ozimos.test.util.StateLoadingAdapterUtil
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MovieActivity : AppCompatActivity() {

    private val binding by lazy { ActivityMovieBinding.inflate(layoutInflater) }
    private val viewModel: MovieViewModel by viewModels()
    private lateinit var dataExtra: GenreItemModel

    private val adapter by lazy { MovieAdapterPaging3() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        getData()
        setObserver()
        setView()
    }

    private fun setView() {
        binding.run {
            rvMovie.adapter = adapter.withLoadStateFooter(footer = StateLoadingAdapterUtil {
                adapter.retry()
            })
            labelMovie.text = getString(R.string.movie_name, dataExtra.name)

            layoutEmpty.btnReload.setOnClickListener {
                adapter.refresh()
            }
            adapter.addLoadStateListener {
                if (it.append.endOfPaginationReached) {
                    if (adapter.itemCount == 0) {
                        layoutEmpty.root.isVisible = true
                    }
                }

                binding.progressBar.isVisible = it.source.refresh is LoadState.Loading
                layoutEmpty.root.isVisible = it.refresh is LoadState.Error
            }
        }
    }

    private fun setObserver() {
        listMovieObserver()
    }

    private fun listMovieObserver() {
        viewModel.moviePaging.observe(this) {
            setDataMovie(it)
        }
    }

    private fun setDataMovie(data: PagingData<MovieModel>) {
        lifecycleScope.launch {
            adapter.submitData(pagingData = data)
        }
        adapter.onclick = {
            DetailMovieActivity().openActivity(this, movieId = it.id.toInt())
        }

    }

    private fun getData() {
        val intentExtra = intent.getStringExtra(DATA_EXTRA_GENRE)
        if (!intentExtra.isNullOrEmpty()) {
            dataExtra = Gson().fromJson(intentExtra, GenreItemModel::class.java)
            getMovie()
        }
    }

    private fun getMovie() {
        viewModel.getMovieByGenrePaging(dataExtra.id)
    }

    companion object {
        const val DATA_EXTRA_GENRE = "DATA_EXTRA_ID"
    }
}