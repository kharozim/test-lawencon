package com.ozimos.test.presentation.review

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.paging.PagingData
import com.google.gson.Gson
import com.ozimos.test.R
import com.ozimos.test.databinding.ActivityReviewBinding
import com.ozimos.test.domain.model.MovieModel
import com.ozimos.test.domain.model.ReviewModel
import com.ozimos.test.presentation.MovieViewModel
import com.ozimos.test.util.StateLoadingAdapterUtil
import com.ozimos.test.util.showToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class ReviewActivity : AppCompatActivity() {

    private val binding by lazy { ActivityReviewBinding.inflate(layoutInflater) }
    private val viewModel: MovieViewModel by viewModels()
    private var dataExtra: MovieModel? = null

    @Inject
    lateinit var adapterReview: ReviewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        getData()
        setObserver()
        setView()
    }

    private fun setView() {
        binding.run {

            tvLabelReview.text = getString(R.string.review_movie, dataExtra?.title)
            rvReview.adapter =
                adapterReview.withLoadStateFooter(StateLoadingAdapterUtil { adapterReview.refresh() })

            layoutEmpty.btnReload.setOnClickListener {
                adapterReview.refresh()
            }

            adapterReview.addLoadStateListener {
                if (it.append.endOfPaginationReached) {
                    layoutEmpty.root.isVisible = adapterReview.itemCount == 0
                }

                binding.progressBar.isVisible = it.source.refresh is LoadState.Loading
                layoutEmpty.root.isVisible = it.refresh is LoadState.Error
                rvReview.isVisible = it.refresh !is LoadState.Error
                if (it.refresh is LoadState.Error) {
                    val message = (it.refresh as LoadState.Error).error.localizedMessage
                    showToast(message ?: "")
                }

            }
        }


    }

    private fun setObserver() {
        viewModel.reviewPaging.observe(this) { pagingReview ->
            setDataReview(pagingReview)
        }
    }

    private fun setDataReview(data: PagingData<ReviewModel>?) {
        lifecycleScope.launch {
            data?.let { paging ->
                adapterReview.submitData(paging)
            }
        }
    }

    private fun getData() {

        val intentExtra = intent.getStringExtra(DATA_EXTRA_MOVIE)
        dataExtra = Gson().fromJson(intentExtra, MovieModel::class.java)

        dataExtra?.let {
            viewModel.getListMovieReview(movieId = it.id.toInt())
        }
    }

    fun openActivity(context: Context, movie: String) {
        val intent = Intent(context, ReviewActivity::class.java)
        intent.putExtra(DATA_EXTRA_MOVIE, movie)
        context.startActivity(intent)
    }

    companion object {
        private const val DATA_EXTRA_MOVIE = "DATA_EXTRA_MOVIE"
    }
}