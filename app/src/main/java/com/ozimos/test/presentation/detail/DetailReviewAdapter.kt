package com.ozimos.test.presentation.detail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.ozimos.test.R
import com.ozimos.test.databinding.ItemDetailReviewBinding
import com.ozimos.test.domain.model.ReviewModel
import com.ozimos.test.util.DiffUtilPaging3
import com.ozimos.test.util.FormatterUtil
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DetailReviewAdapter @Inject constructor() :
    PagingDataAdapter<ReviewModel, DetailReviewAdapter.MyViewHolder>(DiffUtilPaging3()) {

    var onclick: ((ReviewModel) -> Unit)? = null

    inner class MyViewHolder(private val binding: ItemDetailReviewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(item: ReviewModel) {
            binding.run {

                val writtenAt = formatterDate(item.createdAt)

                ivImage.load(item.authorDetails.avatarPathClean) {
                    placeholder(R.drawable.ic_launcher_foreground)
                }
                tvName.text = root.context.getString(R.string.written_by, item.author)
                tvCreatedAt.text = root.context.getString(R.string.written_at, writtenAt)
                tvDescription.text = item.content
                tvRating.text = item.authorDetails.rating.toString()

                root.setOnClickListener { onclick?.invoke(item) }
            }
        }

    }

    private fun formatterDate(createdAt: String): String {
        val toDate = FormatterUtil.stringToDate(createdAt)
        return FormatterUtil.dateToString(toDate, formatTo = "MMMM dd, yyyy")
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            ItemDetailReviewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        getItem(position)?.let { holder.bindData(it) }
    }

}
