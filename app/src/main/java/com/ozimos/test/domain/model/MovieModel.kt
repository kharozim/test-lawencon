package com.ozimos.test.domain.model

import com.ozimos.test.BuildConfig

data class MovieModel(
    override val id: String = "0",
    val overview: String = "",
    val originalLanguage: String = "",
    val originalTitle: String = "",
    val video: Boolean = false,
    val title: String = "",
    val genreIds: List<Int> = emptyList(),
    val posterPath: String = "",
    val backdropPath: String = "",
    val releaseDate: String = "",
    val popularity: Double = 0.0,
    val voteAverage: Double = 0.0,
    val adult: Boolean = false,
    val voteCount: Int = 0
) : BaseModel() {
    val posterPathClean
        get() = BuildConfig.BASE_URL_IMAGE + posterPath
}