package com.ozimos.test.domain.model

abstract class BaseModel {
    abstract val id: String
}