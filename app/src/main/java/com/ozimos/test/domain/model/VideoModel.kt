package com.ozimos.test.domain.model

data class VideoModel(
    val id: Int? = null,
    val results: List<VideoItemItemModel> = emptyList()
)

data class VideoItemItemModel(
    val site: String = "",
    val size: Int = 0,
    val iso31661: String = "",
    val name: String = "",
    val official: Boolean = false,
    override val id: String = "",
    val type: String = "",
    val publishedAt: String = "",
    val iso6391: String = "",
    val key: String = "",
    var isSelected: Boolean = false
) : BaseModel()