package com.ozimos.test.domain.model

data class GenreModel(
    val genres: List<GenreItemModel> = emptyList(),
)

data class GenreItemModel(
    override val id: String = "0",
    val name: String = ""
) : BaseModel()



